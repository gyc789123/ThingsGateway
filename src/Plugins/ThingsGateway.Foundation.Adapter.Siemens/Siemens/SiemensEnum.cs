﻿namespace ThingsGateway.Foundation.Adapter.Siemens
{
    public enum SiemensEnum
    {
        S200,
        S200Smart,

        S300,
        S400,

        S1200,
        S1500,

    }
}