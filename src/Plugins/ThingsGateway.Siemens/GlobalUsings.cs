﻿global using Microsoft.Extensions.DependencyInjection;

global using System.Net;

global using ThingsGateway.Foundation.Adapter.Siemens;
global using ThingsGateway.Web.Foundation;

global using TouchSocket.Core;
global using TouchSocket.Sockets;


